
import pygame
import os

import spatialh

from constants import tile_size, tile_map



class stage():
	def __init__(self,blueprint_name):
		#self.surf = Game_Surface(self.size*)
		self.objects = []
		self.terrain = []
		self.actors = []
		self.particles = []

		self.spawn_points = []
				
		path = os.path.join("stages", blueprint_name + ".bmp")
		blueprint =  pygame.image.load(path)
		
		s = blueprint.get_size()
		size = [s[0]*tile_size,s[1]*tile_size]
		self.size = size		
		self.surf_bgr = pygame.Surface(size)
		self.surf_bgr.fill((0,0,0))
#		self.surf_bgr.fill((1,0,0))

		self.surf_obj = pygame.Surface(size)
		self.surf_obj.set_colorkey((0,0,0))
		self.surf_obj.fill((0,0,0))
		
		self.surf_prt = pygame.Surface(size)
		self.surf_prt.set_colorkey((0,0,0))		
		self.surf_prt.fill((0,0,0))
		#self.surf_bgr.set_colorkey((255,255,255))
		print s
		self.spacehash = spatialh.spatialhash(size[0],size[1],tile_size)

		for x in range(0 , blueprint.get_width() ):
			for y in range(0 , blueprint.get_height() ): #setting walls
				try:
					c = blueprint.get_at(( x , y))[0:3]
					bx = x*tile_size
					by = y*tile_size
					tile_map[c](bx,by,self)
				except KeyError:					
					pass
		for t in self.terrain:
			t.set_flags()		
		for t in self.terrain:
			t.draw()		
		self.LevelRect = pygame.Rect(0,0,size[0],size[1])
		

				
					

