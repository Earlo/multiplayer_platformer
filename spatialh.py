import pygame
import vector

class spatialhash(object):
	def __init__(self,x,y,s):
		self.size = s
		self.xlen = (x/s) + 1
		self.ylen = (y/s) + 1
		self.content = {}
		self.hash = []
		for x in range(0,self.xlen):
			self.hash.append([])
			for y in range (0,self.ylen):
				self.hash[x].append([])
				
	def update(self,obj):
		u_rect = obj.rect.inflate((2,2)) #to take in account how Pygame rects are
		try:
			for ox,oy in self.content[obj]:
				self.hash[ox][oy].remove(obj)
		except KeyError:
			pass
		sx = u_rect.left / self.size
		sy = u_rect.top / self.size
		bx = u_rect.right / self.size
		by = u_rect.bottom / self.size
		cords = []
		for x in range( sx, bx + 1):
			for y in range( sy, by + 1):
				try:
					self.hash[x][y].append(obj)
				except IndexError:
					print x,y,"spa-33"
				cords.append([x,y])
		self.content[obj] = cords
	
	def rect_in_hash(self,rect):
		sx = rect.left / self.size
		sy = rect.top / self.size
		bx = rect.right / self.size
		by = rect.bottom / self.size
		cords = []
		for x in range( sx, bx + 1):
			for y in range( sy, by + 1):
				cords.append([x,y])
		return cords
				
	def line_in_hash(self,line):
		s_h = [int(line[0][0]/self.size), int(line[0][1]/self.size)] #start
		e_h = [int(line[1][0]/self.size), int(line[1][1]/self.size)] #end
		points = []
		issteep = abs(e_h[1]-s_h[1]) > abs(e_h[0]-s_h[0])
		if issteep:
			s_h[0], s_h[1] = s_h[1], s_h[0]
			e_h[0], e_h[1] = e_h[1], e_h[0]
		rev = False
		if s_h[0] > e_h[0]:
			s_h[0], e_h[0] = e_h[0], s_h[0]
			s_h[1], e_h[1] = e_h[1], s_h[1]
			rev = True
		deltax = e_h[0] - s_h[0]
		deltay = abs(e_h[1]-s_h[1])
		error = int(deltax / 2)
		y = s_h[1]
		ystep = None
		if s_h[1] < e_h[1]:
			ystep = 1
		else:
			ystep = -1
		for x in range(s_h[0], e_h[0] + 1):
			if issteep:
				points.append((y, x))
			else:
				points.append((x, y))
			error -= deltay
			if error < 0:
				y += ystep
				error += deltax
		# Reverse the list if the coordinates were reversed
		if rev:
			points.reverse()
		#points.pop(0)
		return tuple(points)

	def debug(self):
		size = self.size
		from game import PROGRAM
		
		for x in range(0,len(self.hash)):
			for y in range(0,len(self.hash[x])):
				if len(self.hash[x][y]) == 0:
					pygame.draw.rect(PROGRAM.level.surf_bgr, (30,30,30),(x*size, y*size ,size ,size),1)
				else:
					pygame.draw.rect(PROGRAM.level.surf_bgr, (230,130,130),(x*size, y*size ,size ,size),1)
			

					


						
def getdircost(loc1,loc2):
	if loc1[0] - loc2[0] != 0 and loc1[1] - loc2[1] != 0:
		return 1.4 # diagnal movement
	else:
		return 1.0 # horizontal/vertical movement
		
def get_h_score(end,current):
	hscore = (abs(end[0]-current[0])+abs(end[1]-current[1])) * 10
	return hscore
	
def closest_enemy(a,hash, searcher): #find closest enemy
	from main import size
	#print "from", a , "to", b
	openset = [] # The set of tentative nodes to be evaluated, initially containing the start node
	closedset = []
	start = [int(a[0])/size,int(a[1])/size] #pos
	# Cost from start along best known path.		
	openset.append(start)
	current = start
	done = False
	while not done:
		closedset.append(current)
		openset.remove(current)
		for os_x,os_y in [[-1,0],[1,0],[0,1],[0,-1]]:
			test = [current[0]+os_x , current[1]+os_y]
			if 0<test[0]<hash.xlen and 0<test[1]<hash.ylen:
				for obj in hash.hash[test[0]][test[1]]:
					if "CHAR" in obj.FLAGS and not obj == searcher:
						obj.colour = [255,0,50]
						return obj
				cond = True
				if not (test in closedset or test in openset):
					openset.append(test)


		current = openset[0]

			
def Astar( a, b, hash): #A* algorithm
	from main import size
	a= [int(a[0])/size,int(a[1])/size]
	b= [int(b[0])/size,int(b[1])/size]
	#print "from", a , "to", b

	openset = [] # The set of tentative nodes to be evaluated, initially containing the start node
	closedset = []
	start = [ [a[0], a[1]] , None , 0 , get_h_score(b,a), get_h_score(b,a)] #pos , parent , g score, h score, f score
	# Cost from start along best known path.		
	openset.append(start)
	current = start
	done = False
	while not done:
		closedset.append(current)
		if current[0] == b:
			done = True
			return current
		openset.remove(current)
		for os_x,os_y in [[-1,0],[1,0],[0,1],[0,-1]]:
			test = [current[0][0]+os_x , current[0][1]+os_y]
			if 0<test[0]<hash.xlen and 0<test[1]<hash.ylen:
				cond = True
				if not test == b:
					for obj in hash.hash[test[0]][test[1]]:
						if "STAT" in obj.FLAGS or "ENV" in obj.FLAGS:
							cond = False
				if cond:
					gscore = getdircost(test,current[0]) + current[2]
					hscore = get_h_score(b, test)
					next = [ [test[0], test[1]] , current , gscore , hscore , gscore+hscore] #pos , parent , g score, h score, f score
					cond = True
					for n in closedset:
						if n[0] == test:
							cond = False
					if cond:
						for n in openset:
							if n[0] == test:
								cond = False
						if cond:
							openset.append(next)
						else:
							if next[2] >= getdircost(test,current[0]) + current[2]:
								next[1] = current
		if len(openset) == 0:
			print "no path"
		try:
			current = min (openset,key=lambda node:node[4])
		except ValueError:
			print openset
		
		
def draw_path(tile): #devbyus
	from main import hash
	done = False
	while not done:
		if tile[1] == None:
			done = True
		else:
			hash.hash[tile[0][0]][tile[0][1]].append("Line")
			tile = tile[1]
	
def find_waypoints( a, b, hash): #debygs
	tile = Astar( a, b, hash)
	from main import hash, surf_bgr
	done = False
	start = tile[0]
	tile = tile[1]

	while not done:
		if tile[1] == None:
			pygame.draw.line(surf_bgr, (255,0,0), [start[0]*hash.size+hash.size/2,start[1]*hash.size+hash.size/2], [tile[0][0]*hash.size+hash.size/2,tile[0][1]*hash.size+hash.size/2], 1)
			done = True
		elif not vector.line_shash([start[0]*hash.size+hash.size/2,start[1]*hash.size+hash.size/2],[tile[1][0][0]*hash.size+hash.size/2,tile[1][0][1]*hash.size+hash.size/2]):
			pygame.draw.line(surf_bgr, (255,0,0), [start[0]*hash.size+hash.size/2,start[1]*hash.size+hash.size/2] ,[tile[1][0][0]*hash.size+hash.size/2,tile[1][0][1]*hash.size+hash.size/2], 1)
			start = tile[1][0]
		else:
			tile = tile[1]
			
def get_waypoint( a, b, hash):
	tile = Astar( a, b, hash)
	if not tile == None:
		from main import hash
		done = False
		start = tile[0]
		if tile[1] == None: #in case on one tile long hashes
			return [tile[0][0]*hash.size+hash.size/2,tile[0][1]*hash.size+hash.size/2]
			done = True
		tile = tile[1]
		while not done:
			if tile[1] == None:
				return [tile[0][0]*hash.size+hash.size/2,tile[0][1]*hash.size+hash.size/2]
				done = True
			elif not vector.line_shash([start[0]*hash.size+hash.size/2,start[1]*hash.size+hash.size/2],[tile[1][0][0]*hash.size+hash.size/2,tile[1][0][1]*hash.size+hash.size/2]):
				#return [tile[1][0][0]*hash.size+hash.size/2,tile[1][0][1]*hash.size+hash.size/2]
				return [tile[0][0]*hash.size+hash.size/2,tile[0][1]*hash.size+hash.size/2]
			else:
				tile = tile[1]
