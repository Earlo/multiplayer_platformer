import math
import pygame
import wrappers
from constants import gravity

import particle
import random

wall_d = {"WALL_L":-1,"WALL_R":1}

class command_wrapper():
	def __init__(self,user):
		self.user = user

	def down(self):
		self.user.walk.down()
		
	def dt_down(self):
		self.user.walk.dt_down()
		self.user.jump.dt_down()
		
		self.user.commands.remove("DT_DOWN")
		
class basic_walk():
	def __init__(self,user):
		self.user = user
	
		self.wall_run = [0,0]

		self.update_values()
		
	def update_values(self):
		
		self.speed = 2.5 + math.sqrt(self.user.WG)/1.3
		self.dd_speed = 2.5 + math.sqrt(self.user.WG)/2 #drop down speed
		self.acl = self.speed / (11. + math.sqrt(self.user.WG)/1.4)
		self.air_control = .2 + self.user.WG/500
		
		if self.user.WG > 10:
			if self.user.WG < 40:
				self.wall_run[0] = 30 / self.speed
			else:
				self.wall_run[0] = ( math.sqrt ( self.user.WG ) * (30 / math.sqrt(40) ) ) / self.speed
		else:
			self.wall_run[0] = 0

			
	def act(self,d):#direction
		self.update_values()
		if "ON_GROUND" in self.user.COL_FLAGS:
			mod = 1
		else:
			mod = self.air_control
		
		change = [0,0]
		ds = self.user.movement_vector[0]
		s = math.fabs(ds)
		#cha_d = ((d and -ds) < 0 or (d and -ds) > 0)
		wr = d == 1 and "ON_WALLR" in self.user.COL_FLAGS or d == -1 and "ON_WALLL" in self.user.COL_FLAGS
		old_wr = d == 1 and "ON_WALLR" in self.user.OLD_COL_FLAGS or d == -1 and "ON_WALLL" in self.user.OLD_COL_FLAGS
		
		#if not wr and self.wall_run[1] > 0:#top reached
		if not wr and old_wr and not "JUMP" in self.user.commands:#top reached, has not walljumpted
			c = math.fabs(self.user.movement_vector[1]) * 0.8
			self.user.movement_vector[1] = self.user.movement_vector[1] * 0.2
			self.user.movement_vector[0] = d * c
			#change[1] = self.user.movement_vector[1] * 0.2
			#change[0] = d * c

			self.wall_run[1] = 0

		sa_d = ((d <= 0 and ds <= 0) or (d >= 0 and ds >= 0))# are we accelerating to the direction we area already going towards
		
		
		if ((s+self.acl*mod) <= self.speed*mod and sa_d) or not sa_d:
			if "UP" in self.user.commands and self.wall_run[0] > self.wall_run[1] and wr: #if can and trying to wallrun
				#if "ON_GROUND" in self.user.COL_FLAGS:
					#change[1] = -self.speed
				self.user.movement_vector[1] = - self.speed
				self.wall_run[1] += 1
			else:
				change[0] += self.acl*d*mod
			
				
			if not sa_d and "ON_GROUND" in self.user.COL_FLAGS: #particle cretion test
				pt = math.fabs(self.user.movement_vector[0])
				if pt > 3.:
					if pt >= random.randrange(3,8): 
						from game import PROGRAM
						v = [ self.user.movement_vector[0] , -.60 ]
						p = particle.dust(self.user.rect.midbottom,v)
						PROGRAM.level.particles.append(p)				
		self.user.movement_vector = map( sum, zip ( self.user.movement_vector, change) )
		
	def down(self):
		pass
		
	def dt_down(self):
		if "ON_GROUND" in self.user.COL_FLAGS:	
			self.user.COL_FLAGS.remove("ON_GROUND")		
			self.user.FLAGS.add("DRP_DOWN") #drop down
			self.user.timers.append([[pygame.time.get_ticks(),100],[wrappers.remove_FLAG,self.user,"DRP_DOWN"]])
			if self.user.movement_vector[1] < self.dd_speed:
				self.user.movement_vector[1] = self.dd_speed  

			
	def stall(self):
		if "ON_GROUND" in self.user.COL_FLAGS:
			ds = self.user.movement_vector[0]
			s = math.fabs(self.user.movement_vector[0])
			if ds == s:
				d = 1
			else:
				d = -1
			self.user.movement_vector[0] -= self.acl*d
			pt = math.fabs(self.user.movement_vector[0]) #particle creation test
			if pt > 4.:
				if pt >= random.randrange(2,8): 
					from game import PROGRAM
					v = [ self.user.movement_vector[0] , -.60 ]
					p = particle.dust(self.user.rect.midbottom,v)
					PROGRAM.level.particles.append(p)
			
			if s < self.acl:
				self.user.movement_vector[0] = 0
				
	def hit_wall(self,w):
		d = wall_d[w]
		ds = self.user.movement_vector[0]
		if (d < 0 and ds < 0) or (d > 0 and ds > 0):
			self.user.movement_vector[0] = 0.
			
	def land(self):
		self.wall_run[1] = 0


class basic_jump():
	def __init__(self,user):
		self.user = user
		self.up_timer = [10,0]
		
		self.wall_jumps =  [0,0]
		self.air_jumps =  [0,0]
		self.air_dash =  [0,0]		
		self.update_values()
			
		self.RELEASED = False
	def update_values(self):
		if self.user.WG < 60:
			self.str = 5. + self.user.WG / 10
			self.up_timer[0] = 30 + self.user.WG / 2 #max of 30, about half a second
		else:
			self.str = 11
			self.up_timer[0] = 60

		
		if self.user.WG >= 20: #WJUMPS
			if self.user.WG >= 50: #maxinium amount of wall jumps
				self.wall_jumps[0] = 6
			else:
				self.wall_jumps[0] = 1 + (self.user.WG-20) / 6 #max 6
		else:
			self.wall_jumps[0] = 0
		
		if self.user.WG >= 35 and self.user.WG <= 75:
			self.air_dash[0] = 0
			if self.user.WG >= 65: #check if payer can use the maxinium amount of air jumps
				self.air_jumps[0] = 2
			else:
				self.air_jumps[0] = 1 + (self.user.WG-35) / 15  #max 6
		elif self.user.WG >= 75: #air_dash
			self.air_jumps[0] = 0
			self.air_dash[0] = (self.user.WG - 70) * 5 #replace with a timer
		else:
			self.air_jumps[0] = 0
			self.air_dash[0] = 0
			
	def act(self,holding):
		self.update_values()
		ground = "ON_GROUND" in self.user.COL_FLAGS
		#self.str = 1. + math.sqrt(self.user.WG)
		has_uptime = self.up_timer[0] > self.up_timer[1]
		on_wall = "ON_WALLL" in self.user.COL_FLAGS or "ON_WALLR" in self.user.COL_FLAGS
		G = gravity/60 * self.user.G_mod
		change_mods = []
		change = [0,0] # total change in momentum up/down
		change[1] += G  #change to be relative to time

			
		if ground and self.up_timer[1] == 0: #start jump
			change[1] -= self.str

		if not holding and not ground and not self.RELEASED: #released midjump
			self.RELEASED = True	

		if holding and self.RELEASED: #air options

			if self.wall_jumps[0] > self.wall_jumps[1] and on_wall: #check walljumps
				#self.user.movement_vector = [0,0]	
				change[1] -= self.str						
				if "ON_WALLL" in self.user.COL_FLAGS:
					self.user.movement_vector[0] += math.sqrt(self.str*2)
				elif "ON_WALLR" in self.user.COL_FLAGS:
					self.user.movement_vector[0] -= math.sqrt(self.str*2)

				self.wall_jumps[1] += 1
				self.up_timer[1] = self.wall_jumps[1]
				
				self.RELEASED = False

									
			elif self.air_jumps[0] > self.air_jumps[1]: #check airjumps
				jd = [0,0]
				if "UP" in self.user.commands:
					jd[1] = -1
				elif "DOWN" in self.user.commands:
					jd[1] = 1
				if "RIGHT" in self.user.commands:
					jd[0] = 1
				elif "LEFT" in self.user.commands:
					jd[0] = -1
				

				if not 0 in jd:
					aj_mod = math.sqrt(2)/2
				else:
					aj_mod = 1
					if jd == [0,0]:
						jd = [0,-1]
					
				#check if there is change of direction, i there is make change absolute, else, relative
				#self.user.movement_vector = [0,0]
				G = 0
				#change[0] += math.sqrt(self.str*2)
				self.user.movement_vector = [jd[0]*aj_mod*self.str , jd[1]*aj_mod*self.str]
				#self.user.movement_vector[0] += math.sqrt(self.str*2)
				self.air_jumps[1] += 1
				self.RELEASED = False
				self.up_timer[1] = self.air_jumps[1]*2
				
			elif self.air_dash[0] > self.air_dash[1]: #check airdashes
				self.RELEASED = False

				jd = [0,0]
				if "UP" in self.user.commands:
					jd[1] = -1
				elif "DOWN" in self.user.commands:
					jd[1] = 1
				if "RIGHT" in self.user.commands:
					jd[0] = 1
				elif "LEFT" in self.user.commands:
					jd[0] = -1
			
				if jd == [0,0]:
					jd = [0,-1]
				if not 0 in jd:
					aj_mod = math.sqrt(2)/2
				else:
					aj_mod = 1
				#check if there is change of direction, i there is make change absolute, else, relative
				#self.user.movement_vector = [0,0]
				G = 0
				#change[0] += math.sqrt(self.str*2)
				self.user
				self.user.movement_vector = [jd[0]*aj_mod*self.str , jd[1]*aj_mod*self.str]
				#self.user.movement_vector[0] += math.sqrt(self.str*2)
				self.air_dash[1] += 1
				#self.up_timer[1] = self.air_jumps[1]*2				
	
		if not ground and on_wall:
			G = G/2. #cut movement in half
			pt = math.fabs(self.user.movement_vector[1]) #particle creation test
			if pt > 5.:
				if pt >= random.randrange(5,25): 
					from game import PROGRAM
					if "ON_WALLL" in self.user.COL_FLAGS:
						v = [ 1. , -self.user.movement_vector[1]*.05 ]
						p = particle.dust(self.user.rect.midleft,v)
						PROGRAM.level.particles.append(p)
					if "ON_WALLR" in self.user.COL_FLAGS:
						v = [ -1. , -self.user.movement_vector[1]*.05 ]
						p = particle.dust(self.user.rect.midright,v)
					PROGRAM.level.particles.append(p)
						
		if not self.RELEASED and has_uptime: #holding jump, going up
			G = 0		
		else: #stopped holding jump button. landing
			self.up_timer[1] = 255
			
		self.up_timer[1] += 1
		
		change[1] += G

		self.user.movement_vector = map( sum, zip ( self.user.movement_vector, change) )
	def dt_down(self):#fast fall
		if not "ON_GROUND" in self.user.COL_FLAGS:
			if self.user.movement_vector[1] < self.str:
				self.user.movement_vector[1] = self.str
										
	def reset(self):

		self.up_timer[1] = 0
		self.wall_jumps[1] = 0
		self.air_jumps[1] = 0
		self.air_dash[1] += 1		
		self.RELEASED = False
			
	def land(self):
		self.user.movement_vector[1] = 0
		self.user.walk.land()
		self.reset()
	def can_land(self):
		return self.user.movement_vector[1] >= 0
	def hit_roof(self):
		if not "ON_GROUND" in self.user.COL_FLAGS:
			if self.user.movement_vector[1] < 0:
				self.user.movement_vector[1] = -self.user.movement_vector[1]
			self.up_timer[1] = self.up_timer[0] + 1		
