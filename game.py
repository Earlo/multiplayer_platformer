import pygame
#import pygtk
#pygtk.require('2.0')
#import gtk
from pygame.locals import *
import os
import sys


from constants import gkey_map,mkey_map

import menu
import stage
import controls
import screen
import wrappers


#from constants import tile_size

class game():
	def __init__(self):
		
		self.FPS = 60 # 60 frames per second
		
		self.SWIDTH =  1000
		self.SHEIGTH = 600	
		self.MainWindow = pygame.display.set_mode((self.SWIDTH, self.SHEIGTH))
		self.MainWindow.set_colorkey((0,0,0))
#		pygame.display.set_caption("Pyy")

		self.mouse = [pygame.mouse.get_pos() , False ,  [0,0] , None ]
		self.clock = pygame.time.Clock()
		
		self.additional_tasks = []

		self.surf_GUI = pygame.Surface((self.SWIDTH, self.SHEIGTH))
		self.surf_GUI.set_colorkey((0,0,0))
		self.updates = []
		self.erase = []
		self.draw = []
		self.extra_variables = {}
		
		self.game_keymap = controls.mcontrols(mkey_map)		
		#path = os.path.join("recourses","cursor.bmp")
		self.selected_stage = "Stage0"
		self.PLAYER = None	
		self.PLAYER_AGENTS = {}
		self.funktion = None
		self.reset_GUI()
		self.done = False
		self.debug_message = ""
	
	def m_loop(self):
		while not self.done:
		
			self.mouse[0] = pygame.mouse.get_pos()
			self.mouse[1] = False
			pygame.event.pump()#
			for event in pygame.event.get(): # User did something
				if event.type == pygame.QUIT: # Closed from X
					print "OK SHOULD SHUT DOWN NOW"
					self.done = True # Stop the Loop
					return 0 #???
					print "should not show up .__.,"
				if event.type == pygame.MOUSEBUTTONUP:
					self.mouse[1] = [True]
					for object in self.GUI:#check if any in game buttons are pressed
						if object.pressed(self.mouse[0]):
							object.act()
				if event.type == pygame.KEYDOWN:
					if not self.active_text_field == None:
						self.active_text_field.update(event)
					self.game_keymap.add_input(event.key)
				
				elif event.type == pygame.KEYUP:
					self.game_keymap.rmv_input(event.key)
					
			self.funktion()
			
			pygame.display.update(self.updates)			
			self.updates = []
			
			self.clock.tick(self.FPS)
			pygame.display.set_caption("FPS: %i" % self.clock.get_fps())

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Start menu setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~						
	def sstart_menu(self):
		self.reset_GUI()
		buttons = []
		buttons.append(menu.button(pygame.Rect(100,50,200,100),"Star Game",[self.slocal_game]))
		buttons.append(menu.button(pygame.Rect(100,160,200,100),"Join Game",[self.smulti_game_connect]))
		buttons.append(menu.button(pygame.Rect(100,270,200,100),"Choose Stage",[self.sstage_menu]))
		menu_bar = menu.menu_box(pygame.Rect(300,50,400,400),buttons,(25,50,100))
		self.GUI.append(menu_bar)
		
		self.MainWindow.fill((100,100,100))
		self.refresh_GUI()
		pygame.display.flip()
		self.funktion = self.start_menu
		
	def start_menu(self):
		pass

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~choosee stage menu~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~			
		
	def sstage_menu(self):
		self.reset_GUI()
		buttons = []

		buttons.append(menu.button(pygame.Rect(25,50,100,50),"Return",[self.sstart_menu]))
		menu_bar = menu.menu_box(pygame.Rect(self.SWIDTH-160,10,150,self.SHEIGTH-20),buttons,(25,50,100))
		self.GUI.append(menu_bar)
		buttons = []
		x = 0
		for file in os.listdir('stages'):
			name = file.split(".")[0]
			buttons.append(menu.button(pygame.Rect(10+100*x,10,80,60),name,[self.change_stage, name, "ONETIME"]))
			x += 1
		menu_bar = menu.menu_box(pygame.Rect(10,10,self.SWIDTH-200,self.SHEIGTH-20),buttons,(25,50,50))
		self.GUI.append(menu_bar)

		self.MainWindow.fill((100,100,100))
		self.refresh_GUI()
		pygame.display.flip()
		self.funktion = self.stage_menu
		
	def stage_menu(self):
		#print self.selected_stage
		pass
		
	def change_stage(self, new):
		#new = new[0]
		if not self.selected_stage == new:
			old = self.selected_stage
			self.selected_stage = new		
			print "default stage changed to",new,"from",old,"\n"
		else:
			print "default stage already is",new,"\n" 	

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~game setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~						
	def slocal_game(self):
		self.player_index = 0 #doesn't change in singleplayer
		self.PLAYER_AGENTS = {}
		self.reset_GUI()
		buttons = []

		print self.selected_stage
		self.load_level()
		p_data = {"num":self.player_index,
				  "commands":set(),
				  "position":self.level.spawn_points[0].pos,
				  "momentum":[0,0]}
		self.set_player(p_data)
		
		self.refresh_GUI()
		PROGRAM.MainWindow.blit(PROGRAM.level.surf_bgr,(0,0),self.MainWindow.get_rect())
		PROGRAM.MainWindow.blit(PROGRAM.level.surf_obj,(0,0),self.MainWindow.get_rect())

		pygame.display.flip()
		self.funktion = self.local_game
		print("local game set up")
		
	def local_game(self):
		for a in self.level.actors: #iterates over all agents
			a.act()
		for p in self.level.particles:
			p.act()
		for f in self.additional_tasks:#other non specified things that should be done
			wrappers.run_command(f)
		self.additional_tasks = []
		if self.camera.update(self.PLAYER.pos): #return true if camera has moved
			#self.MainWindow.blit(self.surf_GUI,(0,0),self.camera) 			 			
			self.MainWindow.blit(self.surf_GUI,(0,0)) 			 			
			pygame.display.flip()
			self.updates = []
		#debug	
		#pygame.display.flip()
		
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Multiplayer connection screen~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	def smulti_game_connect(self):
		self.reset_GUI()
		buttons = []
		r = pygame.Rect(100,100,250,40)
		tb = menu.input_box(r,"IP","127.0.0.1")
		self.extra_variables["IP"] = tb
		buttons.append( tb )
		r = pygame.Rect(100,200,250,40)
		tb = menu.input_box(r,"PORT","31425")
		self.extra_variables["PORT"] = tb
		buttons.append(tb)
		menu_bar = menu.menu_box(pygame.Rect(10,10,self.SWIDTH-200,self.SHEIGTH-20),buttons,(25,50,50))		
		self.GUI.append(menu_bar)
		buttons = []


		#buttons.append(menu.button(pygame.Rect(25,50,100,50),"HOST",[self.smulti_game_lobby_host]))
		buttons.append(menu.button(pygame.Rect(25,110,100,50),"JOIN",[self.smulti_game_lobby_client]))
		buttons.append(menu.button(pygame.Rect(25,170,100,50),"Return",[self.sstart_menu]))

		menu_bar = menu.menu_box(pygame.Rect(self.SWIDTH-160,10,150,self.SHEIGTH-20),buttons,(25,50,100))
		self.GUI.append(menu_bar)
		
				
		self.MainWindow.fill((100,100,100))
		self.refresh_GUI()
		pygame.display.flip()
		self.funktion = self.multi_game_connect

	def multi_game_connect(self):
		pass

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Multiplayer lobby host screen~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	def smulti_game_lobby_client(self):
		
		sys.path.append("Server")

		import player_client 
		ip = self.extra_variables["IP"].string
		port = int(self.extra_variables["PORT"].string)

		print "creating player_client"		
		self.ONLINE_CLIENT = player_client.Client(ip,port)
		self.ONLINE_CLIENT.PROGRAM = self
		print "created player_client"
		
		self.funktion = self.multi_game_lobby
		#self.funktion = self.sonline_game

	def multi_game_lobby(self):
		self.ONLINE_CLIENT.Loop()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~online game setup and loop~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~						
	def sonline_game(self):
		print "STARTING ONLINE GAME"
		self.PLAYER_AGENTS =  {}
		self.reset_GUI()
		buttons = []
		#self.camera = screen.Camera(self.SWIDTH,self.SHEIGTH)
		print self.selected_stage
		self.load_level()

		PROGRAM.MainWindow.blit(PROGRAM.level.surf_bgr,(0,0),self.MainWindow.get_rect())
		PROGRAM.MainWindow.blit(PROGRAM.level.surf_obj,(0,0),self.MainWindow.get_rect())

		pygame.display.flip()
		self.funktion = self.wait_for_others
		print("local game set up, waiting for online")

	def wait_for_others(self):
		self.ONLINE_CLIENT.Loop()
					
	def online_game(self):
		self.ONLINE_CLIENT.Loop()
		for a in self.level.actors: #iterates over all agents
			a.act()
		for f in self.additional_tasks:#other non specified things that should be done
			wrappers.run_command(f)
		self.additional_tasks = []
		if self.camera.update(self.PLAYER.pos): #return true if camera has moved
			pygame.display.flip()
			self.updates = []	
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Wrappers and other management functions~~~~~~~~~~~~~~~~~~~~~~~~~~
	def refresh_GUI(self):
		for object in self.GUI:
			object.draw()
			
	def reset_GUI(self):
		self.surf_GUI.fill((0,0,0))
		#self.surf_GUI = pygame.Surface((self.SWIDTH, self.SHEIGTH))
		self.GUI = []
		self.active_text_field = None
		
	def set_player(self,data):
		key = data["num"]
		c = set(data["commands"])
		p = data["position"]
		m = data["momentum"]

		try:
			self.PLAYER_AGENTS[key].commands = c
			if len(p) == 2:
				self.PLAYER_AGENTS[key].pos = p
			if len(m) == 2:
				self.PLAYER_AGENTS[key].movement_vector = m
		except KeyError:
			from character import character
			new_p = character(self.level.spawn_points[0].pos , self.level)
			new_p.key_num = key 
			self.PLAYER_AGENTS[key] = new_p
			self.PLAYER_AGENTS[key].commands = c
			if len(p) == 2:
				self.PLAYER_AGENTS[key].pos = p
			if len(m) == 2:
				self.PLAYER_AGENTS[key].movement_vector = m	
		
		if self.PLAYER == None:
			try:
				self.PLAYER = self.PLAYER_AGENTS[self.player_index]
				self.game_keymap = controls.gcontrols(self.PLAYER,gkey_map)
				print "PLAYER DEFINED"
				#test
				r = pygame.Rect(4,4,200,20)
				hp_bar = menu.bar(r, "HP", [0,100], c1 = (255,0,0) )
				r = pygame.Rect(4,28,200,20)
				wg_bar = menu.bar(r, "Wager", [0,100],  c1 = (230,102,0) )
				wg_bar = menu.bar(r, "Kaalinkaappaus", [0,100],  c1 = (230,102,0) )
				wg_bar.c1 = (255,102,0)
				self.PLAYER.hp_bar = hp_bar
				self.PLAYER.wg_bar = wg_bar
				
				self.GUI.append(hp_bar)
				self.GUI.append(wg_bar)
		
			except KeyError:
				pass
								
	def load_level(self):
		self.camera = screen.Camera(self.SWIDTH,self.SHEIGTH)
		self.level = stage.stage(self.selected_stage)
				
def start():
	global PROGRAM
	PROGRAM = game()
	PROGRAM.funktion = PROGRAM.sstart_menu
	PROGRAM.m_loop()
	pygame.quit()		




