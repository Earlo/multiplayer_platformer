import math
import pygame
import vector

from constants import tile_size
#from constants import *

wall_dirs = { "GROUND" : lambda obj,env: check_ground (obj,env),
			  "CEILING": lambda obj,env: check_ceiling(obj,env),
			  "WALL_L" : lambda obj,env: check_wallL  (obj,env),
			  "WALL_R" : lambda obj,env: check_wallR  (obj,env)}
			  
wall_keys = set(wall_dirs.keys())
from random import randint as ri

		
def collision_handler(obj):
	pos1 = obj.pos[:]
	pos2 = map(sum, zip(obj.movement_vector, obj.pos))

	from game import PROGRAM
	hash = PROGRAM.level.spacehash
	pos1 = obj.pos[:]
	pos_change = [pos2[0]-pos1[0],pos2[1]-pos1[1]]
	done = False
	if not pos_change == [0,0]:
		vectors = vector.divide_by(pos_change, 2.) #optimaze later
	else:
		vectors = [[0,0]]
	#for c in points:
	while not len(vectors) == 0: 
		c = vectors.pop()
		#[i for i in LONG_LIST if i in SHORT_LIST]
		obj.pos = map(sum, zip(c,obj.pos))
		obj.rect.center = [int(obj.pos[0]),int(obj.pos[1])]
		hash.update(obj)
		objs = set()
		for x,y in hash.content[obj]:
			for obj2 in hash.hash[x][y]:
				if not obj2 == obj:
					objs.add(obj2)
		for obj2 in objs:
			obj.rect.center = [int(obj.pos[0]),int(obj.pos[1])] #keep rect updated
			if "RECT" in obj.FLAGS and "RECT" in obj2.FLAGS:
				if "ENV" in obj2.FLAGS: #if colliding with the stage
					from game import PROGRAM
					PROGRAM.additional_tasks.append([obj2.draw])
					for f in obj2.DIR_FLAGS:
						correction_vector = wall_dirs[f](obj,obj2)
						if not correction_vector == None and not correction_vector == [0,0]:
							#print correction_vector
							vectors.append(correction_vector)
						#obj.rect.center = [int(obj.pos[0]),int(obj.pos[1])] #keep rect updated
				elif obj.rect.colliderect(obj2.rect): #do something here plz. :D
					#npos = rect_rect(obj,obj2)
					old_pos = obj.pos[:]
					rect_rect(obj2,obj)
					
					#de bug
					#obj2.colour = (ri(0,255),ri(0,255),ri(0,255))
					#pos_can[vector.simple_distance(pos1,npos)] = npos
	#				done = True
	#	if done:
			#print "collision",pos1[1],obj.pos[1]
		#	return 0
			
			#	#min_key = min(pos_can.keys())
			#	min_key = min(pos_can.keys())
			#	return pos_can[min_key]
#	obj.pos = pos2
#	print "no objections"
	
def check_ground(obj,env):

#	s = map(sum, zip(obj.rect.bottomleft ,[2, -obj.rect.height / 2]))
#	e = (obj.rect.width-4, 1 + obj.rect.height/2)
#	r = pygame.Rect(s,e)
	from constants import wall_cr
	r = wall_cr["G"](obj.rect)
	if r.colliderect(env.rect):
		if obj.jump.can_land():
			#print obj.FLAGS,env.FLAGS,"FLAGS are here"
			if "PLT" in env.FLAGS: #special conditions for thin platforms
				if "DRP_DOWN" in obj.FLAGS or ("DOWN" in obj.commands and not "ON_GROUND" in obj.OLD_COL_FLAGS):
					return None			
			if not "ON_GROUND" in obj.OLD_COL_FLAGS:
				obj.jump.land()
		#		obj.COL_FLAGS.add("ON_GROUND")
		#		obj.pos[1] = env.rect.top - (obj.rect.height/2)
		#		obj.rect.bottom = env.rect.top
			obj.COL_FLAGS.add("ON_GROUND")
			c =  env.rect.top - (obj.pos[1] + obj.rect.height/2)
			#print c,"from",env.rect.top, (obj.pos[1] + obj.rect.height/2),obj.rect.center
			if c < 0:
				return [0, c] #returns negative

def check_ceiling(obj,env):
#	s = map(sum, zip(obj.rect.topleft ,[2,obj.rect.height / 2]))
#	e = (obj.rect.width-4,-(1 + obj.rect.height / 2 )
#	r = pygame.Rect(s,e)
	from constants import wall_cr
	r = wall_cr["C"](obj.rect)
	r.normalize()
	if r.colliderect(env.rect):
		obj.COL_FLAGS.add("HIT_ROOF")
		obj.jump.hit_roof()
		#obj.pos[1] = env.rect.bottom + (obj.rect.height/2) + 0 # +2 to take account how Pygame Rects work
		#obj.rect.top = env.rect.bottom
		c =  env.rect.bottom - (obj.pos[1] - obj.rect.height/2)
		#print c,"from",env.rect.bottom , (obj.pos[1] , obj.rect.height/2),obj.rect.center
		if c > 0:
			return [0, c] #returns positive


		
def check_wallR(obj,env): #right side of player

#	s = map(sum, zip(obj.rect.topright ,[-obj.rect.width / 2 ,2]))
#	e = ( 2 + obj.rect.width / 2 , obj.rect.height - 4)
#	r = pygame.Rect(s,e)
	from constants import wall_cr
	r = wall_cr["R"](obj.rect)
	r.normalize()
	if r.colliderect(env.rect):
		obj.COL_FLAGS.add("ON_WALLR")
		obj.walk.hit_wall("WALL_R")
		#obj.pos[0] = env.rect.left - (obj.rect.width/2)
		#obj.rect.right = env.rect.left
		c =  env.rect.left - (obj.pos[0] + obj.rect.width/2)
		#print c,"from", env.rect.left , (obj.pos[0] , obj.rect.width/2),obj.rect.center
		if c < 0:
			return [c, 0] #returns positive


		
def check_wallL(obj,env):
#	s = map(sum, zip(obj.rect.topleft ,[obj.rect.width / 2,-2]))
#	e = (-(2 + obj.rect.width / 2) ,obj.rect.height-4)
#	r = pygame.Rect(s,e)
	from constants import wall_cr
	r = wall_cr["L"](obj.rect)
	r.normalize()
	if r.colliderect(env.rect):
		obj.COL_FLAGS.add("ON_WALLL")
		obj.walk.hit_wall("WALL_L")
		#obj.pos[0] = env.rect.right + (obj.rect.width/2) + 0 # +2 to take account how Pygame Rects work
		#obj.rect.left = env.rect.right # +2 to take account how Pygame Rects work		
		#return [obj.rect.left - env.rect.right, 0 ] #returns negative

		c =  env.rect.right - (obj.pos[0] - obj.rect.width/2)
		if c > 0:
			return [c, 0] #returns negative

				
#collision detections and stuff
def circ_circ(A,B,dis):
	overlap = (A.rad+B.rad)-dis
	dir= uvectorl(A.rect.center,B.rect.center)
	len= overlap/2
	if not B.momentum[0] == [0,0] or not A.momentum[0] == [0,0]:
		sum = [A.momentum[0][0]+B.momentum[0][0],A.momentum[0][1]+B.momentum[0][1]]
		A.momentum[0] = [sum[0]/2,sum[1]/2]
		B.momentum[0] = [sum[0]/2,sum[1]/2]
	if "STATIONARY" in A.FLAGS:
		B.pos = vpoint(B.rect.center,dir,-len*2)
	#	B.hashmove()
	#	B.rect.center = [int(B.pos[0]),int(B.pos[1])]
	#	B.hashpend()
	#	for hash in B.hashes:
	#		for item in hash:
	#			if "ENV" in item.FLAGS:
	#				rect_rect(A,B)
	elif "STATIONARY" in B.FLAGS:
		A.pos = vpoint(A.rect.center,dir,len*2)
	else:
		B.pos = vpoint(B.rect.center,dir,-len*2)
		A.pos = vpoint(A.rect.center,dir,len*2)
	
def rect_rect(A,B):
	ar = math.atan2(A.rect.centery - A.rect.top, A.rect.right - A.rect.centerx) # half of the angle of the right side
	# this is normalized into [0, 2] to make searches easier (no negative numbers and stuff)
	dirint = [ 2*ar, math.pi, math.pi+2*ar, 2*math.pi]
	# calculate angle towars the center of the other rectangle, + ar for normalization into
	ad = math.atan2(A.rect.centery - B.rect.centery, B.rect.centerx - A.rect.centerx) + ar
	# again normalization, sincen atan2 ouputs values in the range of [-pi,pi]
	if ad < 0:
		ad = 2*math.pi + ad
	# search for the quadrant we are in and return it
	for i in range(len(dirint)):
		if ad < dirint[i]:
			case = i
			break
	os_x = 0
	os_y = 0
	d_x = None
	d_y = None
	if case == 0: 	#B is on right side -> x axis
		d_x = B.pos[0]-A.pos[0]
		min_dx = (B.rect.width+A.rect.width)/2
		os_x = min_dx - d_x

	elif case == 1: # B is above -> y axis
		d_y = A.pos[1]-B.pos[1]
		min_dy = (B.rect.height+A.rect.height)/2
		os_y = d_y - min_dy

	elif case == 2: # B is on left side -> x axis
		#d_x = A.rect.centerx-B.rect.centerx
		d_x = A.pos[0]-B.pos[0]
		min_dx = (B.rect.width+A.rect.width)/2
		os_x = d_x - min_dx

	else: # B is below -> y axis
		d_y = B.pos[1]-A.pos[1]
		min_dy = (B.rect.height+A.rect.height)/2
		os_y = min_dy - d_y

	if "STATIC" in A.FLAGS:
		B.pos = [B.pos[0] + os_x , B.pos[1] + os_y]
	elif "STATIC" in B.FLAGS:
		A.pos = [A.pos[0] + os_x , A.pos[1] + os_y]
	else:
		B.pos = [B.pos[0] + os_x/2  ,B.pos[1] + os_y/2]
		A.pos = [A.pos[0] + os_x/2 , A.pos[1] + os_y/2]
		
		
def ccw(A,B,C):
	return (C[1]-A[1])*(B[0]-A[0]) > (B[1]-A[1])*(C[0]-A[0])
	
def seg_intersect(A, B): #linesegment intersection	
	return ccw(A[0],B[0],B[1]) != ccw(A[1],B[0],B[1]) and ccw(A[0],A[1],B[0]) != ccw(A[0],A[1],B[1])
	
def seg_rect(seg,rect): #test if linesegment and rectangle intersect
	r_lines = [[rect.bottomleft,rect.bottomright],[rect.bottomright,rect.topright],[rect.topright,rect.topleft],[rect.topleft,rect.bottomleft]]
	#r_lines = [[rect.bottomleft,rect.topright],[rect.bottomright,rect.topleft]]
	
	for line in r_lines:
		if seg_intersect(seg, line):
			return True
	return False		
