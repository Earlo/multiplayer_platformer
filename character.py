import pygame
import math
import wrappers
import physics
import movement


class spawn_point():
	def __init__(self,point,stage):
		self.pos = point
		self.stage = stage
		self.stage.spawn_points.append(self)
		
		
class character(object):
	def __init__(self,pos,stage):
		from game import PROGRAM
	#	PROGRAM.PLAYER_AGENTS.append(self)
	#	PROGRAM.PLAYER = self
		self.HP = 100.
		self.WG = 0
		self.WAGER = 0.
		self.speed = 5.
		self.walk = movement.basic_walk(self)
		self.jump = movement.basic_jump(self)
		self.movement = movement.command_wrapper(self)
		self.pos = pos
		self.movement_vector = [0.,0.]
		self.G_mod = 1 #gravity modifier
		
		self.timers = [] #[[start_t,max_t],[funk,pa,ra,me,ters]]
		self.FLAGS = set(("PLAYER","ACTOR","RECT"))
		self.COL_FLAGS = set()
		self.OLD_COL_FLAGS = set()
		self.rect = pygame.Rect((0,0),(16,24))
		self.rect.center = self.pos
		self.stage = stage
		
		self.stage.actors.append(self)
		self.stage.spacehash.update(self)
		
		self.actions =  {"UP"      :[self.nothing],
						 "DOWN"    :[self.movement.down],
						 "DT_DOWN" :[self.movement.dt_down],
						 "LEFT"    :[self.walk.act,-1 ],
						 "RIGHT"   :[self.walk.act, 1 ],
						 "JUMP"    :[self.jump.act,True], #holding jump button
						 "DT_JUMP" :[self.nothing], 
						 "DROP"    :[self.jump.act,False], #not holding
						 "STALL"   :[self.walk.stall],
						 "ADD_W"   :[self.change_wg, 1],
						 "RDC_W"   :[self.change_wg,-1] }	
		self.commands = set()
		
		self.draw()
	def act(self):
		
		if (not "LEFT" in self.commands) and (not "RIGHT" in self.commands):
			self.commands.add("STALL")
		else:
			try:
				self.commands.remove("STALL")
			except KeyError:
				pass
		if not "ON_GROUND" in self.COL_FLAGS and not "JUMP" in self.commands:
			self.commands.add("DROP") #to make sure
		else:
			try:
				self.commands.remove("DROP")
			except KeyError:
				pass	
		command_keys = self.commands.copy() 
		for key in command_keys:
			c = self.actions[key][:]
			funk = c.pop(0)
			funk(*c)
		n_timers = []
		for timer in self.timers:
			if pygame.time.get_ticks() - timer[0][0] > timer[0][1]: #if timer has been filed
				wrappers.run_command(timer[1])
			else:
				n_timers.append(timer)
		self.timers = n_timers	
		
		self.erase()
		
		#print self.COL_FLAGS		
		self.OLD_COL_FLAGS = self.COL_FLAGS
		self.COL_FLAGS = set()#resetting collisions
		old_pos = self.pos[:]
		physics.collision_handler(self)
		#print "change",( self.pos[0]-old_pos[0], self.pos[1]-old_pos[1] )
		#print self.pos
		
		#self.rect.center = (int(self.pos[0]),int(self.pos[1]))
		self.stage.spacehash.update(self)

		self.draw()
#		self.hp_bar.update()
		if not self.hp_bar.value == int(self.HP):		
			self.hp_bar.value = int(self.HP)
			self.HP = self.hp_bar.update()

		#self.wg_bar.draw()
	def change_wg(self,d):
		self.WG += d		
		self.wg_bar.value = int(self.WG)
		self.WG = self.wg_bar.update()
		
	def nothing(self):
		pass
			
	def draw(self):
		from game import PROGRAM
		pygame.draw.rect(self.stage.surf_obj, (10,45,24), self.rect, 0)
		u_rect = self.rect.copy()
		u_rect.topleft = [self.rect.topleft[0]-PROGRAM.camera.topleft[0],self.rect.topleft[1]-PROGRAM.camera.topleft[1]]

		PROGRAM.MainWindow.blit(self.stage.surf_obj,u_rect.topleft,self.rect) 				
		PROGRAM.updates.append(u_rect)
			
	def erase(self):
		from game import PROGRAM
		pygame.draw.rect(self.stage.surf_obj, (0,0,0), self.rect, 0)
		u_rect = self.rect.copy()
		u_rect.topleft = [self.rect.topleft[0]-PROGRAM.camera.topleft[0],self.rect.topleft[1]-PROGRAM.camera.topleft[1]]
		PROGRAM.MainWindow.blit(self.stage.surf_bgr,u_rect.topleft,self.rect) 				
		PROGRAM.updates.append(u_rect)
		
	def remove(self):
		print"poistetaan :D:D:D:D:D syysysys 108 charachter"
		from game import PROGRAM
		self.erase()
		try:
			del PROGRAM.PLAYER_AGENTS[self.key_num]
		except KeyError:
			pass
		try:
			PROGRAM.level.actors.remove(self)	
		except IndexError:
			pass
		
		
