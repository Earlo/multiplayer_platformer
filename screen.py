import pygame

#pygame.font.init()


class Camera(pygame.Rect):
	cameraSlackX = 40
	cameraSlackY = 40
	def __init__(self, windowWidth, windowHeight):
		super(Camera,self).__init__(0,0,windowWidth, windowHeight)
		
	def update(self, pos):
		#print pos
		from game import PROGRAM
		self.pre_pos = self.topleft
		self.centerx = pos[0]
		self.centery = pos[1]
		# Figure out if rect has exceeded camera slack
		if self.centerx - pos[0] > self.cameraSlackX:
			self.left = pos[0] + self.cameraSlackX #- self.width/2
		elif pos[0] - self.centerx > self.cameraSlackX:
			self.left = pos[0] - self.cameraSlackX #- self.width/2
		if self.centery - pos[1] > self.cameraSlackY:
			self.top = pos[1] + self.cameraSlackY #- self.height/2
		elif pos[1] - self.centery > self.cameraSlackY:
			self.top = pos[1] - self.cameraSlackY #- self.height/2

		# This keeps the camera within the boundaries of the level
		if self.right > PROGRAM.level.LevelRect.right:
			self.right = PROGRAM.level.LevelRect.right
		elif self.left < PROGRAM.level.LevelRect.left:
			self.left = PROGRAM.level.LevelRect.left
		if self.top < PROGRAM.level.LevelRect.top:
			self.top = PROGRAM.level.LevelRect.top
		elif self.bottom > PROGRAM.level.LevelRect.bottom:
			self.bottom = PROGRAM.level.LevelRect.bottom
		#self.topleft = [-self.topleft[0],-self.topleft[1]]
		#DDDDDD
		#PROGRAM.MainWindow.blit(PROGRAM.level.surf_bgr,(0,0),self)
		#PROGRAM.MainWindow.blit(PROGRAM.level.surf_obj,(0,0),self) 		
		#return True	
		#debug		
		if not self.topleft == self.pre_pos:	#screen has moved
			PROGRAM.MainWindow.blit(PROGRAM.level.surf_bgr,(0,0),self)
			PROGRAM.MainWindow.blit(PROGRAM.level.surf_obj,(0,0),self)			
			PROGRAM.MainWindow.blit(PROGRAM.level.surf_prt,(0,0),self)			
			return True
		else: #has not
			return False
