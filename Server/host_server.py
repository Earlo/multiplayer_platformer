import sys

from time import sleep, localtime
from weakref import WeakKeyDictionary

from PodSixNet.Server import Server
from PodSixNet.Channel import Channel

class ClientChannel(Channel):
	"""
	This is the server representation of a single connected client.
	"""
	def __init__(self, *args, **kwargs):
		self.nickname = "anonymous"
		Channel.__init__(self, *args, **kwargs)
		
		self.commands = []
		self.position = []
		self.momentum = []
		self.num = 0
		self.done_local = False
	
	def Close(self):
		self._server.DelPlayer(self)
		
	##################################
	### Network specific callbacks ###
	##################################
		
	def Network_local_setup_done(self,data):
		print "Done in",self.char_index
		self.done_local = True
		#self._server.SendToAll({"action": "message", "message": data['message'], "who": self.nickname})
	
	def Network_nickname(self, data):
		self.nickname = data['nickname']
		self._server.SendPlayers()

	######################
	### GAME CALLBACKS ###	
	#####################
	def Network_share_commands(self,data):
		self.commands = data["commands"]
		self.position = data["position"]
		self.momentum = data["momentum"]
class GameServer(Server):
	channelClass = ClientChannel
	
	def __init__(self, *args, **kwargs):
		Server.__init__(self, *args, **kwargs)
		self.players = WeakKeyDictionary()
		print 'Server starting'
		
		self.done = False		
		self.selected_stage = "Stage0_2p"
	
		#self.funk = self.slobby
		self.funk = self.sgame
	
	def slobby(self): #not uset ATM
		self.funk = self.lobby
		 
	def lobby(self): 
		if len(self.players.keyrefs()) > 1: #hardcoded change later
			self.funk = self.connecting
			print "STARING GAME"
			x = 0
			for p in self.players: #give each player a number
				p.done_local = False
				p.char_index = x
				p.Send({"action": "setup_game", "num": x,"stage":self.selected_stage})
				x += 1
			self.funk = self.connecting
				
	
	def connecting(self): #waits until everyone is connected
		Ready = True
		for p in self.players:
			Ready = Ready and p.done_local
		if Ready: 
			self.funk = self.sgame
			print "everyone is ready"
			for p in self.players: #give each player a number
				p.Send({"action": "start_game"})
			self.funk = self.sgame
			for p in self.players:
				p.done_local = False
				
	def sgame(self):
		self.funk = self.game
		
	def game(self):
		for p1 in self.players: #give each player a number
			for p2 in self.players:
				if not p2 == p1:
					p1.Send({"action": "update_commands", 
							 "num": p2.num,
							 "commands" : p2.commands,
							 "position" : p2.position,
							 "momentum" : p2.momentum })
				
	def Connected(self, channel, addr):
		self.AddPlayer(channel)
	
	def AddPlayer(self, player):
		player.num = len(self.players)
		print "New Player" + str(player.addr),"#"+str(player.num)
		print player
		index = len(self.players)
		p_list = []
		for p2 in self.players.keys():
			p_list.append( {"num":p2.num,
						"commands" : p2.commands,
					   "position" : p2.position,
					   "momentum" : p2.momentum } )
			
		self.players[player] = True
		self.SendPlayers()

		player.Send({"action": "setup_game", "num":index ,"stage":self.selected_stage, "players": p_list })
		#player.Send({"action": "setup_game", "num":index ,"stage":self.selected_stage })
		self.SendToAll( {"action": "test"} )
		#print "players", [p for p in self.players],": )"
	
	def DelPlayer(self, player):
		print "Deleting Player" + str(player.addr)
		self.SendToAll( {"action": "remove_player","num":player.num} )
		del self.players[player]
		self.SendPlayers()
	
	def SendPlayers(self):
		self.SendToAll({"action": "players", "players": [p.nickname for p in self.players]})
	
	def SendToAll(self, data):
		[p.Send(data) for p in self.players]
	
	def Launch(self):
		print "Launched server!!!"
		while not self.done:
			self.funk()
			self.Pump()
			sleep(0.0001)
			
if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "Usage:", sys.argv[0], "host:port"
		print "e.g.", sys.argv[0], "localhost:31425"
	else:
		ip, port = sys.argv[1].split(":")
		s = GameServer(localaddr=(ip, int(port)))
		print "server set up"
		s.Launch()
		print "server shutting down"

