import sys
from time import sleep
from sys import stdin, exit

from PodSixNet.Connection import connection, ConnectionListener


class Client(ConnectionListener):
	def __init__(self, host, port):
		print host, port
		self.Connect((host, port))
		print "Chat client started"
		# get a nickname from the user before starting
			
		self.funk = self.nothing
		self.PROGRAM = None #it's set in the main game file
		
		#self.Char = None # will be set when the game starts
		
	def Loop(self):
		connection.Pump()
		self.funk()
		self.Pump()
	
	def nothing(self):
		pass
		
	def game(self):
		#share local commands
		com = list( self.PROGRAM.PLAYER.commands )
		pos = self.PROGRAM.PLAYER.pos
		mom = self.PROGRAM.PLAYER.movement_vector
		connection.Send({"action": "share_commands", "commands": com, "position": pos, "momentum": mom })
		
	#######################################
	### Network event/message callbacks ###
	#######################################
	def Network_test(self, data):
		x = 0
		for P in self.PROGRAM.PLAYER_AGENTS.values():
			print x,"is in",P.pos
			x += 1

	def Network_players(self, data):
		print "*** players: " + ", ".join([p for p in data['players']])
	
	def Network_message(self, data):
		print data['who'] + ": " + data['message']
	
	def Network_setup_game(self,data):
		print data["num"]

		self.PROGRAM.player_index = data["num"]

		self.PROGRAM.selected_stage = data["stage"]
		self.PROGRAM.load_level()
		self.PROGRAM.funktion = self.PROGRAM.sonline_game
	
#		while self.PROGRAM.PLAYER == None:
#			self.PROGRAM.set_player()
			
		for p in data["players"]:
			self.PROGRAM.set_player(p)
		
		p_data = {"num":self.PROGRAM.player_index,
				  "commands":set(),
				  "position":self.PROGRAM.level.spawn_points[0].pos,
				  "momentum":[0,0]}
		self.PROGRAM.set_player(p_data)
		
		print "STARTING GAME!!!"
		self.PROGRAM.funktion = self.PROGRAM.online_game
		self.funk = self.game

		
	def Network_update_commands(self,data):#!!!
		self.PROGRAM.set_player(data)
		
	def Network_remove_player(self,data):
		key = data["num"]
		p = self.PROGRAM.PLAYER_AGENTS[key]
		p.remove()

	def Network_connected(self, data):
		print "You are now connected to the server"
	
	def Network_error(self, data):
		print 'error:', data['error'][1]
		connection.Close()
	
	def Network_disconnected(self, data):
		print 'Server disconnected'
		exit()

#if len(sys.argv) != 2:
#	print "Usage:", sys.argv[0], "host:port"
#	print "e.g.", sys.argv[0], "localhost:31425"
#else:
#	host, port = sys.argv[1].split(":")
#	c = Client(host, int(port))
#	while 1:
#		c.Loop()
#		sleep(0.001)
