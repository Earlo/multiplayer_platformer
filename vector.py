import math

class Vector2d():
	def __init__(self,x,y):
		self.x = x
		self.y = y
	def vec(self):
		return [self.x,self.y]

def vlen(vec): #returns the length of the vector
	return math.sqrt((vec[0])**2 + (vec[1])**2)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def simple_distance(pos1,pos2): #returns distance between two positions
	return math.sqrt((pos1[0]-pos2[0])**2 + (pos1[1]-pos2[1])**2)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def uvector(s,e): #start and end, returns unitvector pointing from start towards end.
	#print (s,e)
	vecx = (e[0] - s[0])
	vecy = (e[1] - s[1])
	length = (math.sqrt(vecx**2+vecy**2))
	unitvector = (vecx/length, vecy/length)
	return unitvector
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vector(s,e): #start and end, returns vector pointing from start to end.
	return [e[0] - s[0], e[1] - s[1]]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def uvectorl(s,e):  #Unit vector that makes sure there is no division by zero
	vecx = (s[0] - e[0])
	vecy = (s[1] - e[1])
	length = (math.sqrt(vecx**2+vecy**2))
	try:
		unitvector = (vecx/length, vecy/length,length)
	except ZeroDivisionError:
		unitvector = (0,0)
	return unitvector
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def uvturn(dir): #returns a vector pointing in direction dir (degrees)
	dir = math.radians(dir)
	vec = [math.sin(dir),math.cos(dir)]
	return vec
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def uvdir(vec):#returns unit vector pointin at same direction as vec
	len = vlen(vec)
	return [vec[0]/len,vec[1]/len]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vdir(vec): #tell where the vector is pointing in degrees
	return math.degrees(math.atan2(vec[0],vec[1]))
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	
def vpoint(pos,vec,len): #returns point at the end of vector vec of length len
	return [pos[0]+vec[0]*len,	pos[1]+vec[1]*len]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vpointnormal(pos,vec,len): #returns point at the end of vector vec's normal of length len
	return [pos[0]-vec[1]*len,	pos[1]+vec[0]*len]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vvectornormal(vec,len,s): #returns vec vectors normal vector of length len pointing at side s
	return [-vec[1]*len*s,vec[0]*len*s]
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def vangle (v1,v2):#returns angle between vec1 and vec2
	return (v1[0]*v2[0]+v1[1]*v2[1])/(math.sqrt(v1[0]**2+v1[1]**2)*math.sqrt(v1[0]**2+v1[1]**2))
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	
def divide_by(vec,mlen): #returns points along the vec between intervals of mlen
	points = []
	olen = vlen(vec) #original length
	uvec = uvdir(vec)

	for x in range(1,int(olen),int(mlen)):
		points.append([uvec[0]*mlen,uvec[1]*mlen])
	x = len(points)
	points.append([vec[0]-x*uvec[0]*mlen,vec[1]-x*uvec[1]*mlen])
	return points
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def divide_by_cont(vec,mlen): #returns points along the vec between intervals of mlen giving continious line of vectors
	points = []
	olen = vlen(vec) #original length
	uvec = uvdir(vec)
	for x in range(0,int(olen),int(mlen)):
		points.append([uvec[0]*x,uvec[1]*x])
	points.append([vec[0],vec[1]])
	return points
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def ssift(pos,tar,dist): #vector movement from pos towards tar
	vecx = (tar[0] - pos[0])
	vecy = (tar[1] - pos[1])
	v = (vecx*dist,vecy*dist)
	return v
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def sift(pos,tar,len):
	vecx = (tar[0] - pos[0])
	vecy = (tar[1] - pos[1])
	length = math.fabs(math.sqrt(vecx**2+vecy**2))
	unitvector = ((vecx/length)*len, (vecy/length)*len)
	newx = int (round (pos[0] - unitvector[0]))
	newy = int (round (pos[1] - unitvector[1]))
	return (newx,newy)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def clear_sigth_test(s,e): #start, end and the size of the raster
	s_h = s.pos[:]
	e_h = e.pos[:]
	points = []
	issteep = abs(e_h[1]-s_h[1]) > abs(e_h[0]-s_h[0])
	if issteep:
		s_h[0], s_h[1] = s_h[1], s_h[0]
		e_h[0], e_h[1] = e_h[1], e_h[0]
	rev = False
	if s_h[0] > e_h[0]:
		s_h[0], e_h[0] = e_h[0], s_h[0]
		s_h[1], e_h[1] = e_h[1], s_h[1]
		rev = True
	deltax = e_h[0] - s_h[0]
	deltay = abs(e_h[1]-s_h[1])
	error = int(deltax / 2)
	y = s_h[1]
	ystep = None
	if s_h[1] < e_h[1]:
		ystep = 1
	else:
		ystep = -1
	for x in range(s_h[0], e_h[0] + 1):
		if issteep:
			points.append((y, x))
		else:
			points.append((x, y))
		error -= deltay
		if error < 0:
			y += ystep
			error += deltax
	# Reverse the list if the coordinates were reversed
	if rev:
		points.reverse()
	from game import PROGRAM
	sta = PROGRAM.stage
	clear = True
	points.pop(0)
	for point in points:
		if "WALL" in sta.tiles[point[0]][point[1]].FLAGS:
			#print "you aren't seen"
			return False #something is in the way
	return True
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#linesegment in spatialhash, rough test if anything is in the way of start and end.
def straigth_path_test(s,e,char): #start, end and the size of the raster
	s_h = s.pos[:]
	e_h = e.pos[:]
	points = []
	issteep = abs(e_h[1]-s_h[1]) > abs(e_h[0]-s_h[0])
	if issteep:
		s_h[0], s_h[1] = s_h[1], s_h[0]
		e_h[0], e_h[1] = e_h[1], e_h[0]
	rev = False
	if s_h[0] > e_h[0]:
		s_h[0], e_h[0] = e_h[0], s_h[0]
		s_h[1], e_h[1] = e_h[1], s_h[1]
		rev = True
	deltax = e_h[0] - s_h[0]
	deltay = abs(e_h[1]-s_h[1])
	error = int(deltax / 2)
	y = s_h[1]
	ystep = None
	if s_h[1] < e_h[1]:
		ystep = 1
	else:
		ystep = -1
	for x in range(s_h[0], e_h[0] + 1):
		if issteep:
			points.append((y, x))
		else:
			points.append((x, y))
		error -= deltay
		if error < 0:
			y += ystep
			error += deltax
	# Reverse the list if the coordinates were reversed
	if rev:
		points.reverse()
	from game import PROGRAM
	sta = PROGRAM.stage
	clear = True
	path = []
	points.pop(0)
	for point in points:
		path.append(sta.tiles[point[0]][point[1]])
		if "WALL" in sta.tiles[point[0]][point[1]].FLAGS:
			#print "you aren't seen"
			return False #something is in the way
	path.append(None)
	
	char.path = path  #nothing is in the way
	#print "you are seen"

	return True
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def seg_seg_pos(seg1,seg2): #tells where two segments intersect
	try:
		s1 = (seg1[0][1]-seg1[1][1])/(seg1[0][0]-seg1[1][0]) #slope1 change of y relative to x
		s2 = (seg2[0][1]-seg2[1][1])/(seg2[0][0]-seg2[1][0]) #slope2
		print s1,s2
		#sp = seg1[0][0] - seg2[0][0]
		d1 = seg1[1][1] - s1*seg1[1][0]
		d2 = seg2[1][1] - s2*seg2[1][0]
		#print d1,d2
		x = -(d1 - d2)/(s1 - s2)
		y = d2+s2*x
		return (x,y)
	except ZeroDivisionError:
		return None
	#debug
#	print d1+s1*x,"is same as",d2+s2*x
#	print d1+s1*x == d2+s2*x

	

	
def seg_rect_cpos(seg,rect): #test where linesegment and rectangle intersect
	r_lines = [[rect.bottomleft,rect.bottomright],[rect.bottomright,rect.topright],[rect.topright,rect.topleft],[rect.topleft,rect.bottomleft]]
	#r_lines = [[rect.bottomleft,rect.topright],[rect.bottomright,rect.topleft]]
	points = []
	for line in r_lines:
		if seg_intersect(seg, line):
			p = seg_seg_pos(seg,line)
			if not p == None:
				points.append(p)
	return points
	
	
#fuckingfuckfuck, i am just too dumb to get this work : P
#def rect_circ(rect,p,rad):
#	return rect.collidepoint(p) or
#		line_circ((rect.topleft,rect.topright)p,rad) or
#		line_circ((rect.topright,rect.bottomright)p,rad)) or
#		line_circ((rect.bottomright,rect.bottomleft)p,rad) or
#		line_circ((rect.bottomleft,rect.topleft)p,rad)
		
def closest_point_on_seg(seg_a, seg_b, circ_pos):
	seg_v = [seg_b[0]-seg_a[0],seg_b[1]-seg_a[1]]
	pt_v = [circ_pos[0]-seg_a[0],circ_pos[1]-seg_a[1]]
	#if seg_v.len() <= 0:
	#	raise ValueError, "Invalid segment length"
	seg_v_len = vlen(seg_v)
	seg_v_unit = [seg_v[0] / seg_v_len , seg_v[1] / seg_v_len]
	proj = pt_v[0]*seg_v_unit[0]+pt_v[1]*seg_v_unit[1]
	if proj <= 0:
		return seg_a
	if proj >= vlen(seg_v):
		return seg_b
	proj_v = [seg_v_unit[0]*proj,seg_v_unit[1]*proj]
	closest = [proj_v[0] + seg_a[0],proj_v[1] + seg_a[1]]
	return closest

def seg_circ(seg_a, seg_b, circ_pos, circ_rad,width):
	closest = closest_point_on_seg(seg_a, seg_b, circ_pos)
	dist_v = [circ_pos[0]-closest[0],circ_pos[1]-closest[1]]
	print (vlen(dist_v))
	#if dist_v.len() > circ_rad:
	#	return vec(0, 0)
	#if dist_v.len() <= 0:
	#	raise ValueError, "Circle's center is exactly on segment"
	#offset = dist_v / vlen(dist_v) * (circ_rad - vlen(dist_v))
	#print offset
	
def check_relative_position(r1,r2): #checks if the two rects are on top of each other or next to
	rects = [r1,r2]
	smaller_x = min ([r1,r2],key=lambda test:test.left)
	rects.remove(smaller_x)
	bigger_x = rects[0]
	if (smaller_x.left + smaller_x.width) > bigger_x.left:
		return True
	rects = [r1,r2]
	smaller_y = min ([r1,r2],key=lambda test:test.top)
	rects.remove(smaller_y)
	bigger_y = rects[0]	
	if (smaller_y.top + smaller_y.height) > bigger_y.top:
		return False
	return None #if you end up here, the rectangles dont share any pseudoborder with each other.
	
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~CURVES~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def round_points(points):
	npoints = []
	#for i in range(0,len(points)-1):
	#	npoints.append(
def b_spline(points):
	pass
	#eh, fuck it
	
	
	
	
	
	
	
	
	
	
	
	
