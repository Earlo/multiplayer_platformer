import pygame

from physics import seg_rect

from random import randint as ri

from constants import tile_size



class ground(object):
	def __init__(self,pos,stage):
		self.pos = pos
		self.rect = pygame.Rect((0,0),(tile_size,tile_size))
#		self.rect.center = self.pos
		self.rect.topleft = self.pos

		self.FLAGS = set(("ENV","STATIC","RECT"))
		self.DIR_FLAGS = set()
		self.stage = stage
		self.stage.terrain.append(self)
		self.stage.spacehash.update(self)
		#self.colour = (0,0,0)
		self.colour = (225,10,24)
		#self.colour = (ri(0,255),ri(0,255),ri(0,255))
		self.dcolour = (27,255,12)
		#self.draw()
		
	def draw(self):
		from game import PROGRAM
		pygame.draw.rect(self.stage.surf_bgr, self.colour, self.rect, 0)
		#pygame.draw.rect(self.stage.surf_bgr, self.dcolour, self.rect, 1)
		for line in self.lines:
			pygame.draw.line(self.stage.surf_bgr,self.dcolour,line[0],line[1],1)
			
		u_rect = self.rect.copy() #inflate to take edges in acccount
		u_rect.topleft = [self.rect.topleft[0]-PROGRAM.camera.topleft[0],self.rect.topleft[1]-PROGRAM.camera.topleft[1]]
		PROGRAM.MainWindow.blit(self.stage.surf_bgr,u_rect.topleft,self.rect.inflate((1,1))) #inflate by one because pygame rects			
		PROGRAM.updates.append(u_rect.inflate((1,1)))

		
	def set_flags(self):
		s = 2
		tr = self.rect.inflate(s, s)
		tr.center = self.rect.center
		dirs = set(("GROUND","CEILING","WALL_L","WALL_R"))
		ls = tr.center #center lines
		le = map(sum, zip(tr.center  ,[ 0, tile_size]))
		bl = (ls,le)
		le = map(sum, zip(tr.center  ,[ 0, -tile_size]))
		ul = (ls,le)
		le = map(sum, zip(tr.center  ,[-tile_size,0]))
		ll = (ls,le)
		le = map(sum, zip(tr.center  ,[tile_size,0]))
		rl = (ls,le)
		
	#	ls = map(sum, zip(tr.midtop,(0,2))) #high lines
	#	le = map(sum, zip(ls  ,[-tile_size,0]))
	#	ull = (ls,le)
	#	le = map(sum, zip(ls  ,[ tile_size,0]))
	#	url = (ls,le)
		
		dlines = [bl,ul,ll,rl]		
		#debug
		self.lines = []
		ls = map(sum, zip(self.rect.bottomleft,(0,0)))
		le = map(sum, zip(self.rect.bottomright,(0,0)))
		dbl = (ls,le)
		ls = map(sum, zip(self.rect.topleft,(0,0)))
		le = map(sum, zip(self.rect.topright,(0,0)))
		dul = (ls,le)
		ls = map(sum, zip(self.rect.bottomleft,(0,0)))
		le = map(sum, zip(self.rect.topleft,(0,0)))
		dll = (ls,le)
		ls =  map(sum, zip(self.rect.bottomright,(0,0)))
		le = map(sum, zip(self.rect.topright,(0,0)))
		drl = (ls,le)		
		self.lines = [dbl,dul,dll,drl]
		
		cords = self.stage.spacehash.rect_in_hash(tr)
		for x,y in cords:
			objs = self.stage.spacehash.hash[x][y]
			for obj in objs:
				if not obj == self and "ENV" in obj.FLAGS:					
					rtr = obj.rect.inflate(s, s)
#					if rtr.colliderect(tr):
#						obj.colour = map(sum,zip (obj.colour, (20,20,20)))
					if seg_rect(bl,rtr):
						try:
							dirs.remove("CEILING")
							self.lines.remove(dbl)
						except KeyError:
							pass
					if seg_rect(ul,rtr):
						try:
							dirs.remove("GROUND")
							self.lines.remove(dul)
						except KeyError:
							pass
					if seg_rect(ll,rtr):
						try:
							dirs.remove("WALL_R")
							self.lines.remove(dll)
						except KeyError:
							pass
					if seg_rect(rl,rtr):
						try:
							dirs.remove("WALL_L")
							self.lines.remove(drl)
						except KeyError:
							pass
		self.DIR_FLAGS = dirs
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~PLATFORM~~~~~~~~~~~~~~~~~~~~				
class platform(object):
	def __init__(self,pos,stage):
		self.pos = pos
		self.rect = pygame.Rect((0,0),(tile_size,4))
		#self.rect.center = self.pos
		self.rect.topleft = self.pos
		self.FLAGS = set(("ENV","STATIC","RECT","PLT"))
		self.DIR_FLAGS = set()
		self.stage = stage
		self.stage.terrain.append(self)
		self.stage.spacehash.update(self)
		#self.colour = (0,0,0)
		self.colour = (225,10,24)
		#self.colour = (ri(0,255),ri(0,255),ri(0,255))
		self.dcolour = (27,255,12)
		#self.draw()
		
	def draw(self):
		from game import PROGRAM
		pygame.draw.rect(self.stage.surf_bgr, self.colour, self.rect, 0)
		#pygame.draw.rect(self.stage.surf_bgr, self.dcolour, self.rect, 1)
		#debug
		#c = len(self.lines)*50
		for line in self.lines:	
			pygame.draw.line(self.stage.surf_bgr,self.dcolour,line[0],line[1],1)
		#	pygame.draw.line(self.stage.surf_bgr,(0,c,0),line[0],line[1],1)
							
		u_rect = self.rect.copy() #inflate to take edges in acccount
		u_rect.topleft = [self.rect.topleft[0]-PROGRAM.camera.topleft[0],self.rect.topleft[1]-PROGRAM.camera.topleft[1]]
		PROGRAM.MainWindow.blit(self.stage.surf_bgr,u_rect.topleft,self.rect.inflate((1,1))) #inflate by one because pygame rects			
		PROGRAM.updates.append(u_rect.inflate((1,1)))

		
	def set_flags(self):
		s = 2
		tr = self.rect.inflate(s, s)
		tr.center = self.rect.center
#		dirs = set(("GROUND","CEILING","WALL_L","WALL_R"))
		dirs = set((["GROUND"]))

		ls = tr.center
		le = map(sum, zip(tr.center  ,[ 0, tile_size]))
		bl = (ls,le)
		
		le = map(sum, zip(tr.center  ,[ 0, -tile_size]))
		ul = (ls,le)
		
		le = map(sum, zip(tr.center  ,[-tile_size,0]))
		ll = (ls,le)
		
		le = map(sum, zip(tr.center  ,[tile_size,0]))
		rl = (ls,le)
		dlines = [bl,ul,ll,rl]				
		#dlines = [ul]		
		#debug
		self.lines = []
		ls = map(sum, zip(self.rect.bottomleft,(0,0)))
		le = map(sum, zip(self.rect.bottomright,(0,0)))
		dbl = (ls,le)
		ls = map(sum, zip(self.rect.topleft,(0,0)))
		le = map(sum, zip(self.rect.topright,(0,0)))
		dul = (ls,le)
		ls = map(sum, zip(self.rect.bottomleft,(0,0)))
		le = map(sum, zip(self.rect.topleft,(0,0)))
		dll = (ls,le)
		ls =  map(sum, zip(self.rect.bottomright,(0,0)))
		le = map(sum, zip(self.rect.topright,(0,0)))
		drl = (ls,le)		
		self.lines = [dbl,dul,dll,drl]
		#self.lines = [dul]
		cords = self.stage.spacehash.rect_in_hash(tr)
		for x,y in cords:
			objs = self.stage.spacehash.hash[x][y]
			for obj in objs:
				if not obj == self and "ENV" in obj.FLAGS:					
					rtr = obj.rect.inflate(s, s)
#					if rtr.colliderect(tr):
#						obj.colour = map(sum,zip (obj.colour, (20,20,20)))
					if seg_rect(bl,rtr):
						try:
					#		dirs.remove("CEILING")
							self.lines.remove(dbl)
						except (KeyError,ValueError) as e:
							pass
					if seg_rect(ul,rtr):
						try:
							pass
					#		dirs.remove("GROUND")
					#		self.lines.remove(dul)
						except (KeyError,ValueError) as e:
							pass
					if seg_rect(ll,rtr):
						try:
					#		dirs.remove("WALL_R")
							self.lines.remove(dll)
						except (KeyError,ValueError) as e:
							pass
					if seg_rect(rl,rtr):
						try:
					#		dirs.remove("WALL_L")
							self.lines.remove(drl)
						except (KeyError,ValueError) as e:
							pass
		#print dirs
		self.DIR_FLAGS = dirs
				
							
