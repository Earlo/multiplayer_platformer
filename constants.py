#import terrain
#from character import character

from pygame.locals import *

gravity = 9.81
tile_size = 20


from terrain import *
from physics import check_ground, check_ceiling, check_wallL, check_wallR
from character import spawn_point
from pygame import Rect as R

tile_map = { (  0,  0,  0): lambda x,y,level: ground      ((x,y),level),
			 (100,100,100): lambda x,y,level: platform    ((x,y),level),
			 (255,  0,  0): lambda x,y,level: spawn_point ((x,y),level)}
#			 (255,  0,  0): lambda x,y,level: character((x,y),level)}

mkey_map = {}

gkey_map = { K_w    :    "UP" ,
			K_s    :  "DOWN" ,
			K_a    :  "LEFT" ,
			K_d    : "RIGHT" ,
			K_SPACE:  "JUMP" ,
			K_q : "ADD_W",
			K_e : "RDC_W"}

dtap_keys = set(("DOWN","JUMP"))#keys that can be double tapped			
dtap_time = 400 #milliseconds

	
wall_cr = { "G": lambda O: R(map(sum, zip(O.bottomleft ,[2,-O.height / 2])) , (O.width-4,  2 + O.height / 2 ))   ,
			#"C": lambda O: R(map(sum, zip(O.topleft   ,[2,- 2 + O.height / 2])) , (O.width-4,-(1 + O.height / 2 )))  ,
			"C": lambda O: R(map(sum, zip(O.topleft    ,[2, O.height / 2])) , (O.width-4,-(1 + O.height / 2 )))  ,
			"R": lambda O: R(map(sum, zip(O.topright   ,[-O.width / 2 ,2])) , (  2 + O.width / 2 , O.height - 4)),
			#"L": lambda O: R(map(sum, zip(O.topleft   ,[- 2 + O.width / 2 ,2])) , (-(1 + O.width / 2), O.height - 4)) }
			"L": lambda O: R(map(sum, zip(O.topleft    ,[ O.width / 2 ,2])) , (-(1 + O.width / 2), O.height - 4)) }
#print "DONE WITH D:D:D:"


