import pygame
import random

class dust():
	def __init__(self,pos,vec):
		self.pos = pos
		self.vec = vec

		self.r0 = 2
		self.r  = 2
		self.rect = pygame.Rect(0,0,self.r*2,self.r*2)
		self.rect.center = self.pos
		
		self.frames = [0,30]

		self.c0 = [100,100,100,50]
		self.c  = [100,100,100,50]
	
	def act(self):
		self.erase()
		self.pos = map(sum,zip(self.pos,self.vec))
		self.vec[0] = self.vec[0] / 1.075
		roll = random.randrange(0,9)
		if roll == 0:
			add = 1.5
		elif roll == 8:
			add = 1.
		elif roll >= 6:
			add = 0.
		elif roll >= 5:
			add = -0.5
		else:
			add = -1.
		self.pos[1] += add
		self.r = int(self.r0 + self.frames[0] / ( self.frames[1] / 8 ))
		self.rect = pygame.Rect(0,0,self.r*2,self.r*2)
		self.rect.center = self.pos
		
		c = int( self.frames[0]*100 / ( self.frames[1]) )
		l = ( c , c , c , c )
		self.c = map( sum, zip (self.c0, l ))
		self.draw()
		self.frames[0] += 1
		if self.frames[0] > self.frames[1]:
			self.remove()
			
	def draw(self):
		from game import PROGRAM
		
		pygame.draw.circle(PROGRAM.level.surf_prt, self.c, self.rect.center, self.r, 0)
		

		u_rect = self.rect.copy()
		u_rect.topleft = [self.rect.topleft[0]-PROGRAM.camera.topleft[0],self.rect.topleft[1]-PROGRAM.camera.topleft[1]]

		PROGRAM.MainWindow.blit(PROGRAM.level.surf_prt,u_rect.topleft,self.rect) 				
		PROGRAM.updates.append(u_rect)
			
	def erase(self):
		from game import PROGRAM

		pygame.draw.circle(PROGRAM.level.surf_prt, (0,0,0) , self.rect.center , self.r, 0)

		u_rect = self.rect.copy()
		u_rect.topleft = [self.rect.topleft[0]-PROGRAM.camera.topleft[0],self.rect.topleft[1]-PROGRAM.camera.topleft[1]]

		#PROGRAM.MainWindow.blit(PROGRAM.level.surf_prt,u_rect.topleft,self.rect) 				
		PROGRAM.MainWindow.blit(PROGRAM.level.surf_bgr,u_rect.topleft,self.rect) 						
		PROGRAM.updates.append(u_rect)


		
	def remove(self):
		from game import PROGRAM
		self.erase()
		try:
			PROGRAM.level.particles.remove(self)	
		except IndexError:
			pass
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class dash_line():
	def __init__(self,s,e):
		self.pos = pos
		self.vec = vec

		self.s = s
		self.e = e
		self.w = 1
		r0 = map( sum, zip (s , (-1,-1) ))
		e = [ e[0]-r0[0],e[1]-r0[1] ]

		r1 = map( sum, zip (e , (1,1) ))
		
		self.rect = pygame.Rect(r0,r1)
		self.rect.normalize()
		
		self.frames = [0,30]

		self.c0 = [0,100,0,50]
		self.c  = [0,100,0,50]
	
	def act(self):
		self.erase()

		self.r = int(self.r0 + self.frames[0] / ( self.frames[1] / 8 ))
		self.rect = pygame.Rect(0,0,self.r*2,self.r*2)
		self.rect.center = self.pos
		
		c = int( self.frames[0]*100 / ( self.frames[1]) )
		l = ( c , c , c , c )
		self.c = map( sum, zip (self.c0, l ))
		self.draw()
		self.frames[0] += 1
		if self.frames[0] > self.frames[1]:
			self.remove()
			
	def draw(self):
		from game import PROGRAM
		
		pygame.draw.line(PROGRAM.level.surf_prt, self.c , self.s, self.e, self.w)
		

		u_rect = self.rect.copy()
		u_rect.topleft = [self.rect.topleft[0]-PROGRAM.camera.topleft[0],self.rect.topleft[1]-PROGRAM.camera.topleft[1]]

		PROGRAM.MainWindow.blit(PROGRAM.level.surf_prt,u_rect.topleft,self.rect) 				
		PROGRAM.updates.append(u_rect)
			
	def erase(self):
		from game import PROGRAM

		pygame.draw.line(PROGRAM.level.surf_prt, (0,0,0) , self.s, self.e, self.w)

		u_rect = self.rect.copy()
		u_rect.topleft = [self.rect.topleft[0]-PROGRAM.camera.topleft[0],self.rect.topleft[1]-PROGRAM.camera.topleft[1]]

		#PROGRAM.MainWindow.blit(PROGRAM.level.surf_prt,u_rect.topleft,self.rect) 				
		PROGRAM.MainWindow.blit(PROGRAM.level.surf_bgr,u_rect.topleft,self.rect) 						
		PROGRAM.updates.append(u_rect)


		
	def remove(self):
		from game import PROGRAM
		self.erase()
		try:
			PROGRAM.level.particles.remove(self)	
		except IndexError:
			pass
